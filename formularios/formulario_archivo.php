<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>
<body>
<!--
    En la etiqueta form son importantes los siguientes atributos method y action
    method para especificar el método en que se va a enviar el formulario "get" o "post"
    action para especificar el archivo que va a procesar la información
    Cuando se procesan archivos  otro atributo necesario es en enctype="multipart/form-data"
    si no se cuenta con ese atributo PHP no procesara ningun archivo
-->
<form method="post" action="procesar_formulario_archivo.php" enctype="multipart/form-data">
    <!-- form text control -->
    <label class="form-label" for="texto">Nombre:</label>
    <!--
        Cuando se procesa en PHP el formulario este los transforma en un arreglo asociativo
        donde la llave sera el name del input y el valor seta el valor del input
    -->
    <input name="texto" class="form-input " type="text" id="input-text" placeholder="Nombre">

    <!-- files -->
    <label class="form-label" for="archivo1">Archivo 1: </label>
    <input type="file" name="archivo" />
    <br /><br />
    <!-- Botones -->
    <input type='submit' class="btn" value="Enviar"/>
    <input type='reset' class="btn btn-primary" value="limpiar"/>

</form>
</body>
