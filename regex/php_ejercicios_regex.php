<?php
//Realizar una expresión regular que detecte emails correctos.
$check="luis_dominguez@comunidad.unam.mx";
$result=preg_match('/^\S+@\S+\.\S+$/',$check);
echo "Correo: ".$result."<br>";
//$emailPattern = '/^\S+@\S+\.\S+$/';

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$check="ABCD123456EFGHIJ78";
$result=preg_match('/^[ABCD123456EFGHIJ78]{18}$/',$check);
echo "CURP: ".$result."<br>";

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

$wordPattern = '/^[a-zA-Z]{51,}$/';
$check="ABCDEFGHIJKLMNOPQRSTUVWXYzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
$result=preg_match($wordPattern,$check);
echo "Palabras: ".$result."<br>";

//Crea una funcion para escapar los simbolos especiales.
function escapeSpecialChars($string) {
    return preg_quote($string, '/');
}
echo "Escape de caracteres: ";
echo escapeSpecialChars("<>");
echo("<br>");

//Crear una expresion regular para detectar números decimales.
$decimalPattern = '/^\d*\.\d+$/';
$check="12.25";
$result=preg_match($decimalPattern,$check);
echo "Decimales: ".$result."<br>";
?>