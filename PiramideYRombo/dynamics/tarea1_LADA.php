<html>
    <head>
        <title>Pirámide y Rombo</title>
    </head>
    <body>
        <center>
        <?php
            $fil=30; //Número de filas
            $ast="*";
            /*for ($i=1;$i<=$fil;$i++){//Pirámide
                echo $ast;
                echo "<br>";
                $ast.="*";
            }
            $ast="*";*/
            for ($i=1;$i<=$fil;$i++){
                if ($i<=$fil/2){ //Hace la pirámide superior
                    echo $ast;
                    echo "<br>";
                    $ast.="*";
                }
                else { //Convierte la pirámide en un rombo
                    $ast = substr($ast, 0, -1); // Elimina el último carácter
                    echo $ast;
                    echo "<br>";
                }
            }
        ?>
        </center>
    </body>
</html>
