<?php 
// Agregar cookie
$cookie_nombre = 'cookie_php';
$cookie_val = 'valor de la cookie con PHP';
$info = array(1,2,3,4,5);
/* 
El parametro time es en segundos 
Si se pone 0, o se omite, la cookie expirará alfinal de la sesión (al cerrarse el navegador). 
*/
setcookie($cookie_nombre, $cookie_val, '0'); 
setcookie('nombre', serialize($info), '0');
?>
<html>
	<head>
		<title> COOKIES </title>
	</head>
	<body>
		<p>Se han creado las cookies</p>
		<a href='./leer.php'>Leer Cookies</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<a href='./borrar.php'>Borrar Cookies</a>
	</body>
</html>