<?php
/* 
Declaramos las funcioes estas no seran ejecutadas hasta que se manden llamar
se recomienda que las funciones siempre esten hasta arriba del código
*/

//declaramos una funcion que recibe 2 parametros obligatorios
echo "<hr>Declaracion y llamado a una funcion<br />";
function suma($num1, $num2) {
	$res = $num1 + $num2;
	echo "el resultado de la suma es " . $res;
}
function suma2($num1, $num2=10) {
	$res = $num1 + $num2;
	echo "el resultado de la suma es " . $res;
}
function imprime(){
	echo "imprime una variable: ";
	// Notice: Undefined variable: varA
	echo $varA;
}
// variable ambito global
$varA = 3;
function imprime2($varA){
	$varA++;
	echo "varA local: ";
	echo $varA;
}
function imprime3(&$varA){
	$varA++;
	echo "varA local: ";
	echo $varA;
}

// se llaman a la funciones 
suma(2, 4);
echo "<hr>Declaracion y llamado a una funcion parametro opcional<br />";
suma2(5);
echo "<hr>Funcion variable global<br />";
imprime();
echo "<hr>Funcion con parametro por valor<br />";
imprime2($varA);
echo "<br />";
echo "varA global: {$varA}";
echo "<hr>Funcion con parametro por referencia<br />";
imprime3($varA);
echo "<br />";
echo "varA global: {$varA}";